Feature('login');

Scenario('login com sucesso',  ({ I }) => {

   I.amOnPage('http://automationpratice.com.br')
   I.click('Login')
   I.waitForText('Login',10)
   I.fillField('#user','amandaqueiroz316@gmail.com')
   I.fillField('#password','123456')
   I.click('#btnLogin')
   I.waitForText('Login realizado',3)

}).tag('@sucesso')

Scenario('login preenchendo apenas o e-mail',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br') 
    I.click('Login')
    I.waitForText('Login',10)
    I.fillField('#user','amandaqueiroz316@gmail.com')
    I.click('#btnLogin')
    I.waitForText('Senha inválida',3)
 });

 Scenario('login sem digitar email e senha',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br')
    I.click('Login')
    I.waitForText('Login',10)
    I.click('#btnLogin')
    I.waitForText('E-mail inválido',3)

 });

 Scenario('login digitando apenas senha',  ({ I }) => {

    I.amOnPage('http://automationpratice.com.br') 
    I.click('Login')
    I.waitForText('Login',10)
    I.fillField('#password','123456')
    I.click('#btnLogin')
    I.waitForText('E-mail inválido',3)
 });